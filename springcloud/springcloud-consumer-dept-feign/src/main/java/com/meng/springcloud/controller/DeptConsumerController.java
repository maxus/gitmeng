package com.meng.springcloud.controller;

import com.meng.springcloud.pojo.Dept;
import com.meng.springcloud.service.DeptClientService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

/**
 * @author mwb
 * @version createtime: 2021/8/24 9:30
 */
@RestController
public class DeptConsumerController {

    @Autowired
    private DeptClientService deptClientService = null;

    @RequestMapping("/consumer/add")
    public boolean add(Dept dept){
        return this.deptClientService.add(dept);
    }
    @RequestMapping("/consumer/get/{id}")
    public Dept get(@PathVariable("id") Long id){
        return this.deptClientService.queryById(id);
    }
    @RequestMapping("/consumer/list")
    public List<Dept> querAll(){
        return this.deptClientService.queryAll();
    }
}
