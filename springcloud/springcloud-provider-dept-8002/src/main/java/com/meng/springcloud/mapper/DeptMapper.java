package com.meng.springcloud.mapper;

import com.meng.springcloud.pojo.Dept;
import org.apache.ibatis.annotations.Mapper;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * @author mwb
 * @version createtime: 2021/8/23 20:37
 */
@Mapper
@Repository
public interface DeptMapper {
    public boolean addDept(Dept dept);
    //public boolean addDept(String dname);
    public Dept queryDeptById(Long id);
    public List<Dept> queryAll();
}
