package com.meng.springcloud.service;

import com.meng.springcloud.mapper.DeptMapper;
import com.meng.springcloud.pojo.Dept;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * @author mwb
 * @version createtime: 2021/8/23 20:48
 */
@Service
public class DeptServiceImpl implements DeptService{
    @Autowired
    private DeptMapper deptMapper;

    @Override
    public boolean addDept(Dept dept) {
        return deptMapper.addDept(dept);
    }
//
//    @Override
//    public boolean addDept(String dname) {
//        return deptMapper.addDept(dname);
//    }

    @Override
    public Dept queryDeptById(Long id) {
        return deptMapper.queryDeptById(id);
    }
    @Override
    public List<Dept> queryAll() {
        return deptMapper.queryAll();
    }
}
