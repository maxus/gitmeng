package com.meng.springcloud.service;

import com.meng.springcloud.pojo.Dept;

import java.util.List;

/**
 * @author mwb
 * @version createtime: 2021/8/23 20:47
 */
public interface DeptService {
    public boolean addDept(Dept dept);
    //public boolean addDept(String dname);
    public Dept queryDeptById(Long id);
    public List<Dept> queryAll();
}
