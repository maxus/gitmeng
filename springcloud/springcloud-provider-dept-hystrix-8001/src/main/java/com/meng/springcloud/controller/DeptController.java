package com.meng.springcloud.controller;

import com.meng.springcloud.pojo.Dept;

import com.meng.springcloud.service.DeptService;
import com.netflix.hystrix.contrib.javanica.annotation.HystrixCommand;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cloud.client.ServiceInstance;
import org.springframework.cloud.client.discovery.DiscoveryClient;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

/**
 * @author mwb
 * @version createtime: 2021/8/23 20:50
 */
// 提供RestFull 服务
@RestController
public class DeptController {
    @Autowired
    private DeptService deptService;

    @GetMapping("/dept/get/{id}")
    @HystrixCommand(fallbackMethod = "hystrixGet") // 当出现问题，就会跳到备选方法
    public Dept get(@PathVariable("id") Long id){
        Dept dept = deptService.queryDeptById(id);
        if(dept==null){
            throw new RuntimeException("id=>"+id+",不存在");
        }
        return dept;
    }
    // 备选方案
    public Dept hystrixGet(@PathVariable("id") Long id){
        return new Dept()
                .setDeptno(id)
                .setDname("id=>"+id+",没有对应的信息，null~@Hystrix")
                .setDb_source("no this database in Mysql");
    }
}
