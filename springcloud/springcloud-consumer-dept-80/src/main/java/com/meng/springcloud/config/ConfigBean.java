package com.meng.springcloud.config;

import org.springframework.cloud.client.loadbalancer.LoadBalanced;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.client.RestTemplate;

/**
 * @author mwb
 * @version createtime: 2021/8/24 9:33
 */
@Configuration
public class ConfigBean {
    // 配置负载均衡实现RestTemplate
    @Bean
    @LoadBalanced //ribbon
    public RestTemplate getTemplate(){
        return new RestTemplate();
    }
}
