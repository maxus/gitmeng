package com.meng.myrule;

import com.netflix.loadbalancer.IRule;
import com.netflix.loadbalancer.RandomRule;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

/**
 * @author mwb
 * @version createtime: 2021/8/25 10:07
 */
@Configuration
public class MengRule {

    @Bean
    public IRule myRule(){
        return new MengRandomRule();// 改为我们自定义的算法，每个服务执行5次
    }
}
