package com.meng.springcloud.pojo;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.experimental.Accessors;

import java.io.Serializable;

/**
 * @author mwb
 * @version createtime: 2021/8/23 19:59
 */
@Data
// @AllArgsConstructor 不能使用注解无参，不然add不进去
@Accessors(chain = true) //链式写法
public class Dept implements Serializable {
    private Long deptno;
    private String dname;
    // 这个数据库是存在哪个数据库的字段~ 微服务架构，一个服务对应一个数据库,
    // 同一个信息可能存在不同的数据库
    private String db_source;

    public Dept(){}

    public Dept(String dname){
        this.dname=dname;
    }
}
