package com.meng.springcloud;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.netflix.zuul.EnableZuulProxy;

/**
 * @author mwb
 * @version createtime: 2021/8/26 10:57
 */
@SpringBootApplication
@EnableZuulProxy
public class ZuulApplication_9223 {
    public static void main(String[] args) {
        SpringApplication.run(ZuulApplication_9223.class,args);
    }
}
